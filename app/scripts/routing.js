import VueRouter from 'vue-router'
import store from './store'

const router = new VueRouter({
    routes: [
        {
            path: '/overview',
            components: {
                default: require('../templates/workspace.vue'),
                topBar: require('../templates/topBar.vue'),
                leftBar: require('../templates/leftBar.vue'),
                rightBar: require('../templates/rightBar.vue')
            },
            beforeEnter: (to, from, next) => {
                store.commit('editModeFlag', false);
                next();
            }
        },
        {
            path: '/login',
            component: require('../templates/login.vue')
        },
        {
            path: '/edit',
            components: {
                default: require('../templates/workspace.vue'),
                topBar: require('../templates/topBar.vue'),
                rightBar: require('../templates/rightBar.vue')
            },
            beforeEnter: (to, from, next) => {
                store.commit('editModeFlag', true);
                next();
            }
        }
        ,
        {
            path: '*',
            component: require('../templates/login.vue')
        }
    ]
});

export {VueRouter, router}
