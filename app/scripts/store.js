import Vue from 'vue/dist/vue.common'
import Vuex from 'vuex';

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        authenticated: true,
        editMode: false,
        dropdownFlag: false
    },
    mutations: { //YOU NEED TO CHANGE STATE'S VALUE. USE "store.commit('FUNCTION',true)" TO RUN IT
        editModeFlag (store, flag) {
            store.editMode = flag;
        },
        dropdownToggle(store, flag){
            store.dropdownFlag = flag;
        }
    },
    getters: {
        //TO COMPUTE STATE BASED ON STORE STATE. YOU NEED TO GET STATE WITHOUT CHANGE ITS VALUE
        // doneTodos: state => {
        //     return state.todos.filter(todo => todo.done)
        // }
    }
});

export default store;