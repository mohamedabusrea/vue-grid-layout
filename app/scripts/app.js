import Vue from 'vue/dist/vue.common'
import store from './store';
import {VueRouter, router} from './routing';

Vue.use(VueRouter);

/*router.beforeEach((to, from, next) => {
 REDIRECT TO LOGIN IF NOT LOGGED
 if (to.path !== '/login' && !store.state.authenticated) {
 next('/login');
 }
 //REDIRECT TO OVERVIEW IF LOGGED
 else if (to.path === '/login' && store.state.authenticated) {
 next('/overview');
 }
 //OPEN NEW PAGE IF NOT LOGGED
 else {
 next();
 }
 next();
 });*/

new Vue({
    store,
    router,
    computed: {
        currentPage: function() {
            return `container--${(this.$route.path).split('/')[1]}`
        }
    },
    mounted(){
        this.cleanBody();
    },
    methods: {
        toggleFlags(){
            store.commit('dropdownToggle', false); //CLOSE DROPDOWN LIST
        },
        cleanBody(){
            document.addEventListener('click', function() {
                $('.customDropdown').addClass('hide_display');
                $('tr').removeClass('bg-colorBlue');
            })
        },
        callDropdown(e){
            const dropdownElement = $(e.currentTarget).siblings(`.customDropdown`);

            if (!dropdownElement.hasClass('hide_display')) { //CLOSE DROPDOWN
                dropdownElement.addClass('hide_display'); //CLICK ON THE SAME BUTTON AGAIN
            }
            else {//OPEN DROPDOWN
                $('.customDropdown').addClass('hide_display'); //FIRST TIME TO CLICK ON THE BUTTON
                dropdownElement.removeClass('hide_display');
            }
        },
        callDropdownRow(e){
            const dropdownElement = $(e.currentTarget).parents('.workspace__widgetDataContent').find(`.customDropdown--tableRow`),
                parentOffset = $(e.currentTarget).parents('.workspace__widgetDataContent').offset();

            $(e.currentTarget).addClass('bg-colorBlue').siblings().removeClass('bg-colorBlue')
            $('.customDropdown').addClass('hide_display'); //HIDE OTHER DROPDOWNS
            dropdownElement.removeClass('hide_display'); //SHOW THIS DROPDOWN
            dropdownElement.css({left: `${e.pageX - parentOffset.left}px`, top: `${e.pageY - parentOffset.top + 40}px`});
        },
        customTableStyle (){
            $('.customTable').each(function() {
                if (!$(this).find('.customTable__fixed').length > 0) {//CHECK IF TI'S ALREADY FIXED
                    let clonedTable = $(this).find('.customTable__data').clone();
                    clonedTable.attr('class', 'customTable__fixed');
                    clonedTable.find('tbody').remove();
                    $(this).prepend(clonedTable);
                }
            })
        },
        customScroll(){
            $('.customScrollH').mCustomScrollbar({theme: 'minimal', scrollInertia: 500, axis: 'x'});
            $('.customScroll').mCustomScrollbar({theme: 'minimal', scrollInertia: 500, axis: 'y'});
        },
        callNotification(opt){
            {
                text, type, timeout, delay
            }
        }
    }
}).$mount('#container');