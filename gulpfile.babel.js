import gulp from 'gulp';
import browserSync from 'browser-sync';
import del from 'del';
import runSequence from 'run-sequence';
import wiredeps from 'wiredep'; // AUTO INJECT BOWER COMPONENTS TO THE HTML
import gulpLoadPlugins from 'gulp-load-plugins'; // AUTO INJECT BOWER COMPONENTS TO THE HTML

const $ = gulpLoadPlugins(),
    wiredep = wiredeps.stream, // AUTO INJECT BOWER COMPONENTS TO THE HTML
    cheerio = require('gulp-cheerio'),
    browserify = require('browserify'), // AUTO INJECT BOWER COMPONENTS TO THE HTML
    vueify = require('vueify'), //INJECT .VUE TEMPLATES IN THE COMPONENTS
    babelify = require("babelify"),
    source = require('vinyl-source-stream'), //THIS JUST CONVERTS THE BUNDLE INTO THE TYPE OF STREAM GULP IS EXPECTING
    buffer = require('vinyl-buffer'), //THIS JUST CONVERTS THE BUNDLE INTO THE TYPE THAT UGLIFY PLUGIN CAN UNDERSTAND
    path = {dev: "app/", tmp: ".tmp/", build: "dist/"},
    env = {dev: $.environments.development, prod: $.environments.production},
    reload = browserSync.reload;

gulp.task('env_dev', () => $.environments.current(env.dev));
gulp.task('env_prod', () => $.environments.current(env.prod));
gulp.task('clean:build', () => del([path.build + '*']));
gulp.task('clean:tmp', () => del([path.tmp + '*']));
gulp.task('clean:js', () => del([path.tmp + 'scripts/']));
gulp.task('clean:fonts', () => {
    return del([path.tmp + 'fonts/*']);
});
gulp.task('copy:fonts', ['clean:fonts'], () => {
    return gulp.src(['bower_components/**/*.{eot,svg,ttf,woff,woff2}', path.dev + 'fonts/*'])
               .pipe($.flatten())
               .pipe(gulp.dest(path.tmp + 'fonts/'));
});
gulp.task('copy:fonts-dist', () => {
    return gulp.src(['bower_components/**/*.{eot,svg,ttf,woff,woff2}', path.dev + 'fonts/*'])
               .pipe($.flatten())
               .pipe(gulp.dest(path.build + 'fonts/'));
});
gulp.task('clean:imgs', () => {
    return env.dev(del([path.tmp + 'images/']))
});
gulp.task('copy:imgs', ['clean:imgs'], () => {
    return gulp.src([path.dev + 'images/**/*'])
               .pipe($.plumber())
               .pipe(env.dev(gulp.dest(path.tmp + 'images/')))
               .pipe(env.prod($.imagemin({
                   progressive: true,
                   interlaced: true
               })))
               .pipe($.size({title: 'images'}))
               .pipe(env.prod(gulp.dest(path.build + 'images/')))
               .pipe(env.dev(reload({
                   stream: true
               })));
});
gulp.task('copy:imgs-dist', () => {
    return gulp.src(path.dev + 'images/**/*')
               .pipe($.plumber())
               .pipe($.imagemin())
               .pipe(gulp.dest(path.build + 'images/'));
});
gulp.task('copy:js', () => {
    return browserify({entries: path.dev + 'scripts/app.js', debug: true})
        .transform(babelify, {presets: ["es2015"]})
        .transform(vueify)
        .bundle()
        .pipe($.plumber())
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(env.dev($.sourcemaps.init({loadMaps: true})))
        .pipe(env.prod($.uglify()))
        .on('error', $.util.log)
        .pipe(env.dev($.sourcemaps.write('./')))
        .pipe(gulp.dest(path.tmp + 'scripts/'));
});
gulp.task('watch', () => {
    gulp.watch(path.dev + '**/*.html', ['html', 'copy:js']);
    gulp.watch(path.dev + '**/*.vue', ['copy:js', 'html']);
    gulp.watch(path.dev + 'styles/*.scss', ['sass']);
    gulp.watch(path.dev + 'styles/**/*.scss', ['sass']);
    gulp.watch(path.dev + 'scripts/*.js', ['copy:js']);
    gulp.watch(path.dev + 'images/**/*', ['copy:imgs']);
    gulp.watch([
        // path.tmp + '*.html',
        path.tmp + 'scripts/*.js'
        // path.dev + '/images/**/*'
    ]).on('change', reload);
});
gulp.task('browserSync', () => {
    browserSync.init({
        server: {
            baseDir: path.tmp,
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });
})
gulp.task('svgstore', () => {
    return gulp.src(path.dev + 'images/svgs/icons/*')
               .pipe($.plumber())
               .pipe($.svgmin(function(file) {
                   return {
                       plugins: [{
                           cleanupIDs: {
                               minify: true
                           }
                       }]
                   }
               }))
               .pipe($.svgstore())
               .pipe($.rename('icons-set.svg'))
               .pipe(cheerio({
                   run: function($) {
                       $('[fill]').removeAttr('fill');
                       $('[class]').removeAttr('class');
                       $('defs').remove();
                       $('svg').addClass('hide_display');
                   },
                   parserOptions: {xmlMode: true}
               }))
               .pipe($.size({title: 'icons-set'}))
               .pipe(gulp.dest(path.dev + 'images/svgs/'));
});
gulp.task('sass', () => {
    return gulp.src(path.dev + 'styles/*.scss')
               .pipe($.plumber())
               .pipe(wiredep()) //TO IMPORT BOOTSTRAP SASS FROM BOWER FOLDER INTO LAYOUT.SCSS
               //.pipe($.sourcemaps.init())
               .pipe($.sass().on('error', $.sass.logError))
               .pipe($.autoprefixer({
                   browsers: ['> 0%'],
                   remove: false
               }))
               //.pipe($.sourcemaps.write('.'))
               .pipe(gulp.dest(path.tmp + 'styles/'))
               .pipe(browserSync.stream());
});
gulp.task('html', () => { //RENDER HTML
    return gulp.src(path.dev + '*.html')
               .pipe($.plumber())
               .pipe(env.dev($.changed(path.tmp, {
                   extension: '.html'
                   // hasChanged: changed.compareSha1Digest
               })))
               .pipe($.nunjucksRender({
                   path: ['./'],
                   envOptions: {
                       tags: {
                           blockStart: '{%',
                           blockEnd: '%}',
                           variableStart: '{$',
                           variableEnd: '$}',
                           commentStart: '{#',
                           commentEnd: '#}'
                       }
                   }
               }))
               .pipe(wiredep()) //IMPORT BOWER PACKAGES INTO HTML
               .pipe(env.prod($.useref())) //CONCATENATE JS / CSS FILES AND MINIFY THEM
               .pipe(env.prod($.if('*.js', $.uglify())))
               .pipe(env.prod($.if('*.css', $.cleanCss())))
               .pipe(env.prod($.if('*.html', $.htmlmin({collapseWhitespace: true}))))
               .pipe(env.dev(gulp.dest(path.tmp)))
               .pipe(env.prod(gulp.dest(path.build)));
});
gulp.task('serve', () => {
    runSequence('clean:tmp', 'env_dev', 'sass', 'copy:js', 'html', 'copy:imgs', 'copy:fonts', 'watch', 'browserSync');
});
gulp.task('build', () => {
    runSequence('clean:build', 'env_prod', 'sass', 'copy:js', 'html', ['copy:imgs-dist', 'copy:fonts-dist']);
});
